var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
	connection.query(`SELECT * FROM users`, function(err, results) {
		if(err) throw err;
		res.status(200).json(results);
	});
});

router.post('/', function(req, res, next) {
	connection.query(`INSERT INTO users SET ?`, req.body, function(err, results) {
		if(err) throw err;
		connection.query("SELECT * FROM users ORDER BY id DESC LIMIT 1", function(err, results) {
			if(err) throw err;
			res.status(201).json(results)
		})
	})
})

router.put('/:id', function(req, res, next) {
	let name = req.body.name;
	connection.query(`UPDATE users SET name='${name}' WHERE id='${req.params.id}'`, function(err, result){
		if(err) throw err;
		connection.query(`SELECT * FROM users WHERE id='${req.params.id}'`, function(err, result) {
			if(err) throw err;
			res.status(200).json(result[0]);
		});
	})
});

router.delete('/:id', function(req, res, next) {
	connection.query(`DELETE FROM users WHERE id=${req.params.id}`, function(err, results) {
		if(err) throw err;
		res.status(200).json({
			message: 'success'
		})
	})
})

module.exports = router;
